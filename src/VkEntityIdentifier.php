<?php

namespace Drupal\vk_entity_identifier;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Class VkEntityIdentifier - Provides methods to identify entities.
 *
 * @package Drupal\vk_entity_identifier
 */
class VkEntityIdentifier {

  /**
   * The entity types that can be identified.
   *
   * @var array
   */
  protected static array $identifiableEntities = [];

  /**
   * The current identifier.
   *
   * @var string
   */
  protected static string $currentIdentifier = '';

  /**
   * Gets the identifier by the entity id.
   *
   * @param string $type
   *   The entity type.
   * @param int $id
   *   The entity id.
   *
   * @return string|null
   *   The identifier or null if not found.
   */
  public static function getIdentifierById(string $type, int $id): ?string {
    try {
      $identifiers = \Drupal::database()
        ->select($type . '__vk_identifier', 'i')
        ->fields('i', ['vk_identifier_value'])
        ->condition('i.entity_id', $id)
        ->condition('i.deleted', 0)
        ->execute()
        ->fetchAssoc();

      return !empty($identifiers['vk_identifier_value']) ? $identifiers['vk_identifier_value'] : NULL;

    }
    catch (\Exception $e) {

      \Drupal::logger('entity_identifier')
        ->warning("Exception while getting identifier by id: @message", ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Gets the identifier on an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string|null
   *   The identifier or null if not found.
   */
  public static function getIdentifier(EntityInterface $entity): ?string {
    return self::entityTypeIsIdentifiable($entity->getEntityTypeId()) && !$entity->vk_identifier->isEmpty()
      ? $entity->vk_identifier->value
      : NULL;
  }

  /**
   * Gets a single entity identified.
   *
   * @param string $identifier
   *   The identifier.
   * @param null $type
   *   The entity type.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity or null if not found.
   */
  public static function getEntity(string $identifier, string $type = NULL): ?EntityInterface {
    $entities = self::getEntities($identifier);
    if (empty($entities)) {
      return NULL;
    }

    if (empty($type) || !isset($entities[$type])) {
      $type = key($entities);
    }
    return reset($entities[$type]);
  }

  /**
   * Gets the Url object from a given identifier.
   *
   * @param string $identifier
   *   The identifier.
   * @param array $options
   *   The options.
   *
   * @return \Drupal\Core\Url|null
   *   The Url object or null if not found.
   */
  public static function getUrlObject(string $identifier, array $options = []): ?Url {
    $entity = self::getEntity($identifier);

    if (!empty($entity)) {
      $url = $entity->toUrl();
      $url->setOptions($options);
      return $url;
    }
    return NULL;
  }

  /**
   * Returns a relative url for a given entity.
   *
   * @param string $identifier
   *   The identifier.
   * @param array $options
   *   The options.
   *
   * @return string
   *   The relative url.
   */
  public static function getUrl(string $identifier, array $options = []): string {
    $urlObject = self::getUrlObject($identifier, $options);
    return !empty($urlObject) ? $urlObject->toString() : '';
  }

  /**
   * Gets all the entities identified by the identifier in parameter.
   *
   * The array is indexed by entity types.
   *
   * @param string $identifier
   *   The identifier.
   *
   * @return array
   *   The entities.
   */
  public static function getEntities(string $identifier): array {
    $entitiesByType = [];
    foreach (self::getIdentifiableEntityTypes() as $type) {
      $entities = self::getEntitiesByType($identifier, $type);
      if (!empty($entities)) {
        $entitiesByType[$type] = $entities;
      }
    }
    return $entitiesByType;
  }

  /**
   * Gets all the identified entities, filtered by $type.
   *
   * @param string $identifier
   *   The identifier.
   * @param string $type
   *   The entity type.
   *
   * @return null|EntityInterface[]
   *   The entities or null if not found.
   */
  public static function getEntitiesByType(string $identifier, string $type): ?array {
    // Attempt to load the field storage for the 'vk_identifier'
    // to ensure it exists for the entity type.
    $field_storage = FieldStorageConfig::loadByName($type, 'vk_identifier');
    if (empty($field_storage)) {
      return NULL;
    }

    // Create an entity query for the specified type,
    // with a condition on the 'vk_identifier' field.
    $query = \Drupal::entityQuery($type)
      ->accessCheck(TRUE)
      ->condition('vk_identifier.value', $identifier);
    $entity_ids = $query->execute();
    if (empty($entity_ids)) {
      return NULL;
    }

    // Load the entities in bulk.
    $loaded_entities = \Drupal::entityTypeManager()->getStorage($type)->loadMultiple($entity_ids);

    // Get the current language ID.
    $current_language_id = \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Attempt to translate each loaded entity
    // to the current language, if applicable.
    foreach ($loaded_entities as &$entity) {
      if ($entity->getEntityTypeId() === 'user' || !($entity instanceof TranslatableInterface)) {
        // Skip translation for user entities or non-translatable entities.
        continue;
      }

      if ($entity->language()->getId() !== $current_language_id && $entity->hasTranslation($current_language_id)) {
        $entity = $entity->getTranslation($current_language_id);
      }
    }

    return $loaded_entities;
  }

  /**
   * Lists the entity types that can be identified.
   *
   * @return array
   *   The entity types.
   */
  public static function getIdentifiableEntityTypes(): array {
    if (empty(self::$identifiableEntities)) {
      self::$identifiableEntities = \Drupal::config('vk_entity_identifier.settings')
        ->get('identifiable_entities');
    }
    return self::$identifiableEntities;
  }

  /**
   * Checks if an entity type can be identified.
   *
   * @param string $type
   *   The entity type.
   *
   * @return bool
   *   true if the entity type can be identified, false otherwise.
   */
  public static function entityTypeIsIdentifiable(string $type): bool {
    return in_array($type, self::getIdentifiableEntityTypes());
  }

  /**
   * Gets the current identifier.
   *
   * @return string|null
   *   The identifier or null if not found.
   */
  public static function getCurrentIdentifier(): ?string {
    // If the current identifier is already set,
    // return it immediately to avoid reprocessing.
    if (self::$currentIdentifier !== '') {
      return self::$currentIdentifier;
    }

    // Iterate through the current route parameters to find an entity.
    foreach (\Drupal::routeMatch()->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        // Once an entity matching the interface is found,
        // determine its identifier.
        $identifier = self::getIdentifier($param);
        if (!empty($identifier)) {
          self::$currentIdentifier = $identifier;
          return $identifier;
        }
      }
    }

    // If no matching entity is found, the identifier remains an empty string.
    self::$currentIdentifier = '';
    return NULL;
  }

}
