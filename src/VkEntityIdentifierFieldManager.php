<?php

namespace Drupal\vk_entity_identifier;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Class VkEntityIdentifierFieldManager - Provides methods to manage fields.
 *
 * @package Drupal\vk_entity_identifier
 */
class VkEntityIdentifierFieldManager {

  /**
   * Creates the field storage config.
   *
   * @param string $type
   *   The entity type.
   *
   * @return \Drupal\field\Entity\FieldStorageConfigInterface|null
   *   The field config entity if one exists for the provided field name,
   *    otherwise NULL.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected static function createFieldStorageConfig(string $type): ?FieldStorageConfigInterface {
    $field_storage = FieldStorageConfig::loadByName($type, 'vk_identifier');

    if (empty($field_storage)) {

      $field = [
        'entity_type' => $type,
        'field_name' => 'vk_identifier',
        'type' => 'string',
        'module' => 'text',
        'settings' => [],
        'cardinality' => 1,
        'locked' => FALSE,
        'indexes' => [],
      ];

      FieldStorageConfig::create($field)->save();
      $field_storage = FieldStorageConfig::loadByName($type, 'vk_identifier');
    }

    return $field_storage;
  }

  /**
   * Creates the field config.
   *
   * @param string $type
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return \Drupal\field\Entity\FieldConfigInterface|null
   *   The field config entity if one exists for the provided field
   *    name, otherwise NULL.
   */
  public static function createFieldConfig(string $type, string $bundle): ?FieldConfigInterface {
    self::createFieldStorageConfig($type);

    $field_config = FieldConfig::loadByName($type, $bundle, 'vk_identifier');

    if (empty($field_config)) {

      $config = [
        'field_name' => 'vk_identifier',
        'entity_type' => $type,
        'bundle' => $bundle,
        'label' => 'Identifier',
        'required' => FALSE,
        'default_value' => [],
        'settings' => [],
      ];

      FieldConfig::create($config)->save();
      $field_config = FieldConfig::loadByName($type, $bundle, 'vk_identifier');
    }

    return $field_config;
  }

  /**
   * Deletes the field config.
   *
   * @param string $type
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected static function deleteFieldConfig(string $type, string $bundle): void {
    $field = FieldConfig::loadByName($type, $bundle, 'vk_identifier');

    if (!empty($field)) {
      $field->delete();
    }
  }

  /**
   * Deletes the field storage config.
   *
   * @param string $type
   *   The entity type.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown when an error occurs while deleting the field storage config.
   */
  public static function deleteFieldStorageConfig(string $type): void {
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($type);

    if (!empty($bundles)) {
      foreach ($bundles as $bundle_name => $bundle_info) {
        self::deleteFieldConfig($type, $bundle_name);
      }

      $field_storage = FieldStorageConfig::loadByName($type, 'vk_identifier');
      if (!empty($field_storage)) {
        $field_storage->delete();
      }
    }
  }

}
