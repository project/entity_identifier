<?php

namespace Drupal\vk_entity_identifier\Controller;

use Drupal\vk_entity_identifier\VkEntityIdentifier;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for the vk_entity_identifier module.
 *
 * @package Drupal\vk_entity_identifier\Controller
 */
class VkEntityIdentifierController {

  /**
   * Redirects to the entity page.
   *
   * @param string $identifier
   *   The identifier.
   * @param string|null $type
   *   The type.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function doRedirect(string $identifier, string $type = NULL): RedirectResponse {
    $entity = VkEntityIdentifier::getEntity($identifier, $type);

    if (!empty($entity)) {
      return new RedirectResponse($entity->toUrl()->toString());
    }
    throw new NotFoundHttpException();
  }

}
