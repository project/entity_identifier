<?php

namespace Drupal\vk_entity_identifier\TwigExtensions;

use Drupal\vk_entity_identifier\VkEntityIdentifier;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides a Twig extension to get the url of an entity.
 *
 * @package Drupal\vk_entity_identifier\TwigExtensions
 */
class VkEntityIdentifierTwig extends AbstractExtension {

  /**
   * Gets the Twig functions.
   *
   * @return array
   *   The Twig functions.
   */
  public function getFunctions() {
    return [
      new TwigFunction('vk_entity_identifier_url', [
        $this,
        'getEntityUrl',
      ], ['is_safe' => ['html']]),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   The extension name.
   */
  public function getName() {
    return 'vk_entity_identifier';
  }

  /**
   * Gets the url of an entity.
   *
   * @param string $identifier
   *   The identifier.
   *
   * @return string
   *   The url of the entity.
   */
  public static function getEntityUrl(string $identifier): string {
    return VkEntityIdentifier::getUrl($identifier);
  }

}
