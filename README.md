# Entity identifier

The VK Entity Identifier module optimizes the way Drupal developers 
interact with and manage entities, making it an essential tool for 
targeted entity manipulation and enhanced theming capabilities. 
By enabling precise identification of individual entities within 
Drupal's CMS, this module facilitates a more intuitive and efficient 
development process, allowing for the customization and thematic 
differentiation of specific content pieces.


For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_identifier).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_identifier).


## Requirements 

This module requires the following module:

- [Field Group](https://www.drupal.org/project/field_group)


## Installation 

Install as you would normally install a contributed Drupal module. 
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration 

1. Enable the module at Administration > Extend.
2. Profit of an enhanced developer experience by 
flagging your first entity (node or taxonomy term).


## FAQ

**Q: How can I use the module to flag entities?**

**A:** After enabling the module, you can flag entities by editing 
existing nodes or taxonomy terms and adding a slug in the "Identifier" field.


## Maintainers 

- Julien Grossio - [jgrossio](https://www.drupal.org/u/jgrossio)
- Denis Carl - [vkarl](https://www.drupal.org/u/vkarl)
